package cz.vojtisek.demo.liftsim.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import cz.vojtisek.demo.liftsim.connector.Connector;
import cz.vojtisek.demo.liftsim.connector.IncomingHandler;
import cz.vojtisek.demo.liftsim.utils.Constants;
import cz.vojtisek.demo.liftsim.utils.Log;

public abstract class BaseActivity extends AppCompatActivity
        implements IncomingHandler.Callbacks {

    private boolean mIsVisible = false;

    @Override
    protected void onResume() {
        super.onResume();
        Log.v(this, "onResume");
        mIsVisible = true;
        Connector.getInstance()
                .setCallbacks(this)
                .doBindService(this);
    }

    @Override
    protected void onPause() {
        Log.v(this, "onPause");
        mIsVisible = false;
        Connector.getInstance()
                .setCallbacks(null);
        super.onPause();
    }

    protected abstract void applyNewState(int stateId);

    @Override
    public void onNewState(int stateId) {
        if (mIsVisible)
            applyNewState(stateId);
    }

    @Override
    public void onRideStart() {
        Log.d(this, "onRideStart");
        onNewState(Constants.STATE_RIDE);
    }

    @Override
    public void onRideFinish() {
        Log.wtf(this, "onRideFinish");
        onNewState(-1);
    }

    @Override
    public void onError(int newStateId) {
        Log.e(this, "onError " + newStateId);
        onNewState(newStateId);
    }

    @Override
    public void onOk() {
        Log.wtf(this, "onOk");
        onNewState(-1);
    }

    @Override
    public void onServiceConnected() {
        //NOOP
    }

    @Override
    public void onServiceDisconnected() {
        startActivity(new Intent(this, DisconnectedActivity.class));
        finish();
    }
}
