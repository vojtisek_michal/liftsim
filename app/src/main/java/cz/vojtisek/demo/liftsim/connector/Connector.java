package cz.vojtisek.demo.liftsim.connector;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import cz.vojtisek.demo.liftsim.utils.Constants;
import cz.vojtisek.demo.liftsim.utils.Log;

public class Connector implements ServiceConnection {

    private static final Connector sInstance = new Connector();

    /**
     * Messenger for communicating with service.
     */
    Messenger mService = null;
    /**
     * Flag indicating whether we have called bind on the service.
     */
    boolean mIsBound;

    /**
     * Target we publish for clients to send messages to Handler.
     */
    IncomingHandler.Callbacks mCallbacks;
    IncomingHandler mHandler = new IncomingHandler();
    Messenger mMessenger = new Messenger(mHandler);

    private Connector() {
    }

    public static Connector getInstance() {
        return sInstance;
    }

    public Connector setCallbacks(IncomingHandler.Callbacks callbacks) {
        sInstance.mCallbacks = callbacks;
        sInstance.mHandler.setListener(callbacks);
        return sInstance;
    }

    public boolean sendMessage(int msg) {
        Log.v(sInstance, "sendMessage " + msg);
        try {
            Message message = Message.obtain(null, msg);
            message.replyTo = mMessenger;
            sInstance.mService.send(message);
            return true;
        } catch (RemoteException e) {
            Log.e(sInstance, "RemoteException", e);
            // There is nothing special we need to do if the service
            // has crashed.
        }
        return false;
    }

    public boolean isConnected() {
        return sInstance.mIsBound && sInstance.mService != null;
    }

    public void doBindService(Context context) {
        if (sInstance.isConnected()) {
            Log.d(sInstance, "doBindService: already bound and have a service connected");
            return;
        }
        Log.d(sInstance, "doBindService");
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        Intent intent = new Intent("com.adleritech.android.developertest.SimulatorService");
        intent.setPackage("com.adleritech.android.developertest");
        sInstance.mIsBound = context.getApplicationContext().bindService(intent,
                sInstance, Context.BIND_AUTO_CREATE);
        //not bound = we there is nobody to connect to
        if (!sInstance.mIsBound && sInstance.mCallbacks != null) {
            //let's tell the callbacks we have been disconnected
            sInstance.mCallbacks.onServiceDisconnected();
        }
    }

    public void doUnbindService(Context context) {
        if (sInstance.mIsBound) {
            Log.d(sInstance, "doUnbindService");
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            /*if (mService != null) {
                try {
                    Message msg = Message.obtain(null,
                            MessengerService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                } catch (RemoteException e) {
                    // There is nothing special we need to do if the service
                    // has crashed.
                }
            }*/

            // Detach our existing connection.
            context.getApplicationContext().unbindService(sInstance);
            sInstance.mIsBound = false;
        } else {
            Log.e(sInstance, "doUnbindService: NOT mIsBound");
        }
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.d(sInstance, "onServiceConnected");
        // This is called when the connection with the service has been
        // established, giving us the service object we can use to
        // interact with the service.  We are communicating with our
        // service through an IDL interface, so get a client-side
        // representation of that from the raw service object.
        sInstance.mService = new Messenger(service);
        if (sInstance.mCallbacks != null) {
            sInstance.mCallbacks.onServiceConnected();
        }

        // We want to monitor the service for as long as we are
        // connected to it.
        try {
            Message msg = Message.obtain(null,
                    Constants.MSG_OUT_REGISTER);
            msg.replyTo = mMessenger;
            sInstance.mService.send(msg);
        } catch (RemoteException e) {
            Log.e(sInstance, "RemoteException", e);
            // In this case the service has crashed before we could even
            // do anything with it; we can count on soon being
            // disconnected (and then reconnected if it can be restarted)
            // so there is no need to do anything here.
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.e(sInstance, "onServiceDisconnected");
        // This is called when the connection with the service has been
        // unexpectedly disconnected -- that is, its process crashed.
        sInstance.mService = null;
        if (sInstance.mCallbacks != null) {
            sInstance.mCallbacks.onServiceDisconnected();
        }
    }
}
