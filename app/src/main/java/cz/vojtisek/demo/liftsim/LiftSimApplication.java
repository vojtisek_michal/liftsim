package cz.vojtisek.demo.liftsim;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

public class LiftSimApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        LeakCanary.install(this);
    }
}
