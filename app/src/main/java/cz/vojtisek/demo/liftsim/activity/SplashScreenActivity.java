package cz.vojtisek.demo.liftsim.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import cz.vojtisek.demo.liftsim.R;
import cz.vojtisek.demo.liftsim.utils.Log;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(this, "onCreate");
        setContentView(R.layout.activity_splash_screen);
    }
}
