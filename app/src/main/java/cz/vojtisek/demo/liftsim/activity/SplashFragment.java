package cz.vojtisek.demo.liftsim.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import cz.vojtisek.demo.liftsim.R;
import cz.vojtisek.demo.liftsim.connector.Connector;
import cz.vojtisek.demo.liftsim.connector.IncomingHandler;
import cz.vojtisek.demo.liftsim.utils.Constants;
import cz.vojtisek.demo.liftsim.utils.Log;

/**
 * Fragment with setRetainInstance(true) to be sure
 * we bind the service only once and we ignore config changes.
 * This is not necessary since we are checking the Connector state
 * and everything could be in {@link SplashScreenActivity}.
 */
public class SplashFragment extends Fragment implements IncomingHandler.Callbacks {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(this, "onCreate");

        setRetainInstance(true);

        Connector.getInstance()
                .setCallbacks(this);
        if (savedInstanceState == null) {
            if (Connector.getInstance().isConnected()) {
                Connector.getInstance()
                        .sendMessage(Constants.MSG_OUT_GET_STATE);
            } else {
                Connector.getInstance().
                        doBindService(getActivity());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ImageView v = new ImageView(getContext());
        v.setImageResource(R.drawable.ic_navigation_primary_light_48dp);
        v.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        return v;
    }

    private void finishSplashToActivity(final int newStateId) {
        Intent i = new Intent(getActivity(),
                MainActivity.class);
        i.putExtra(MainActivity.EXTRA_NEW_STATE, newStateId);
        startActivity(i);

        // close this activity
        getActivity().finish();

        Log.v(this, "finishSplashToActivity " + newStateId);
    }

    @Override
    public void onNewState(int stateId) {
        Log.v(this, "onNewState " + stateId);
        finishSplashToActivity(stateId);
    }

    @Override
    public void onRideStart() {
        Log.wtf(this, "onRideStart");
        finishSplashToActivity(Constants.STATE_RIDE);
    }

    @Override
    public void onRideFinish() {
        Log.wtf(this, "onRideFinish");
        finishSplashToActivity(-1);
    }

    @Override
    public void onError(int newStateId) {
        Log.wtf(this, "onError " + newStateId);
        finishSplashToActivity(newStateId);
    }

    @Override
    public void onOk() {
        Log.wtf(this, "onOk");
        finishSplashToActivity(-1);
    }

    @Override
    public void onServiceConnected() {
        //NOOP
    }

    @Override
    public void onServiceDisconnected() {
        startActivity(new Intent(getActivity(), DisconnectedActivity.class));
        getActivity().finish();
    }


}