package cz.vojtisek.demo.liftsim.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import cz.vojtisek.demo.liftsim.R;
import cz.vojtisek.demo.liftsim.connector.Connector;
import cz.vojtisek.demo.liftsim.utils.Constants;
import cz.vojtisek.demo.liftsim.utils.Log;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    public static final String EXTRA_NEW_STATE = "NEW_STATE";
    private static final int REQUEST_CODE_CUSTOM = 1;

    private Button mButton;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(this, "onCreate");
        setContentView(R.layout.activity_main);

        mButton = (Button) findViewById(android.R.id.button1);
        mProgressBar = (ProgressBar) findViewById(android.R.id.progress);

        mButton.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v(this, "onResume");
        if (getIntent() != null && getIntent().hasExtra(EXTRA_NEW_STATE)) {
            /**
             * Read EXTRA given by Splash Activity.
             */
            applyNewStateFromExtras(getIntent());
        } else {
            /**
             * We don't have any state, this happens when user is in activity started for result
             * and presses home button.
             * When launched again from launcher icon, no splash screen is alive
             * nor other activity in task history.
             */
            Log.wtf(this, "No STATE from Splash Activity nor from broadcasting or riding");
            onNewState(-1);
        }
    }

    @Override
    protected void onDestroy() {
        Log.v(this, "onDestroy");
        Connector.getInstance()
                .doUnbindService(this);
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        final int id = v.getId();
        switch (id) {
            case android.R.id.button1:
                mButton.setEnabled(false);
                mProgressBar.setVisibility(View.VISIBLE);
                Connector.getInstance()
                        .sendMessage(Constants.MSG_OUT_ON_BROADCAST);
                break;
        }
    }

    @Override
    protected void applyNewState(int stateId) {
        Log.d(this, "applyNewState " + stateId);
        mButton.setEnabled(true);
        mProgressBar.setVisibility(View.GONE);

        Intent i = new Intent();
        i.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        switch (stateId) {
            case Constants.STATE_BROADCASTING:
                i.setClass(this, BroadcastingActivity.class);
                break;
            case Constants.STATE_RIDE:
                i.setClass(this, RidingActivity.class);
                break;
            case Constants.STATE_WAITING:
                //NOOP
                return;
            default:
                Connector.getInstance()
                        .sendMessage(Constants.MSG_OUT_GET_STATE);
                return;
        }
        startActivityForResult(i, REQUEST_CODE_CUSTOM);
    }

    private void applyNewStateFromExtras(Intent data) {
        Log.v(this, "applyNewStateFromExtras");

        int newStateId = -1;
        if (data != null) {
            newStateId = data.getIntExtra(EXTRA_NEW_STATE, -1);
        }
        onNewState(newStateId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((resultCode != RESULT_OK)
                && (REQUEST_CODE_CUSTOM == requestCode)) {

            applyNewStateFromExtras(data);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onOk() {
        Log.d(this, "onOk");
        onNewState(Constants.STATE_BROADCASTING);
    }
}
