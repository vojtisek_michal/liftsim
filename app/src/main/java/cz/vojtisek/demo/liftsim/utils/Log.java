package cz.vojtisek.demo.liftsim.utils;


import cz.vojtisek.demo.liftsim.BuildConfig;

public class Log {

    private static final String LOG_TAG = "";

    private static final boolean isLoggable = BuildConfig.DEBUG;

    public static void e(Object obj, String s) {
        if (isLoggable)
            android.util.Log.e(obj.getClass().getName(), s);
    }

    public static void e(Object obj, String s, Throwable t) {
        if (isLoggable)
            android.util.Log.e(obj.getClass().getName(), s, t);
    }

    public static void i(Object obj, String s) {
        if (isLoggable)
            android.util.Log.i(obj.getClass().getName(), s);
    }

    public static void d(Object obj, String s) {
        if (isLoggable)
            android.util.Log.d(obj.getClass().getName(), s);
    }

    public static void v(Object obj, String s) {
        if (isLoggable)
            android.util.Log.v(obj.getClass().getName(), s);
    }

    public static void w(Object obj, String s) {
        if (isLoggable)
            android.util.Log.w(obj.getClass().getName(), s);
    }

    public static void wtf(Object obj, String s) {
        if (isLoggable)
            android.util.Log.wtf(obj.getClass().getName(), s);
    }
}
