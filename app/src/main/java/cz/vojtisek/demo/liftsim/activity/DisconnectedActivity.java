package cz.vojtisek.demo.liftsim.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.jakewharton.processphoenix.ProcessPhoenix;

import cz.vojtisek.demo.liftsim.R;
import cz.vojtisek.demo.liftsim.utils.Log;

public class DisconnectedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(this, "onCreate");
        setContentView(R.layout.activity_disconnected);

        findViewById(android.R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProcessPhoenix.triggerRebirth(DisconnectedActivity.this);
            }
        });
    }
}
