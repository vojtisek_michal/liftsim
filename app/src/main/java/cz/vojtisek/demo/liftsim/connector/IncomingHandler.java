package cz.vojtisek.demo.liftsim.connector;

import android.os.Handler;
import android.os.Message;

import cz.vojtisek.demo.liftsim.utils.Constants;
import cz.vojtisek.demo.liftsim.utils.Log;

/**
 * Handler of incoming messages from service.
 */
public class IncomingHandler extends Handler {

    private Callbacks mListener;

    public IncomingHandler() {
    }

    public void setListener(Callbacks msg) {
        mListener = msg;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case Constants.MSG_IN_STATE:
                Log.v(this, "MSG_IN_STATE " + msg.arg1);
                //mCallbackText.setText("Received from service: " + msg.arg1);
                if (mListener != null)
                    mListener.onNewState(msg.arg1);
                    /*switch (msg.arg1) {
                        case Constants.STATE_WAITING:
                            setWaitingState();
                            break;
                        case Constants.STATE_BROADCASTING:
                            setBroadcastingState();
                            break;
                        case Constants.STATE_RIDE:
                            setRideState();
                            break;
                    }*/
                break;
            case Constants.MSG_IN_ON_RIDE_START:
                Log.v(this, "MSG_IN_ON_RIDE_START");
                if (mListener != null)
                    mListener.onRideStart();
                break;
            case Constants.MSG_IN_ON_RIDE_FINISH:
                Log.v(this, "MSG_IN_ON_RIDE_FINISH");
                if (mListener != null)
                    mListener.onRideFinish();
                break;
            case Constants.MSG_IN_ERROR:
                Log.v(this, "MSG_IN_ERROR, MSG_IN_STATE " + msg.arg1);
                if (mListener != null)
                    mListener.onError(msg.arg1);
                break;
            case Constants.MSG_IN_OK:
                Log.v(this, "MSG_IN_OK");
                if (mListener != null)
                    mListener.onOk();
                break;
            default:
                Log.e(this, "UNKNOWN MESSAGE " + msg.what);
                super.handleMessage(msg);
        }
    }

    public interface Callbacks {
        void onNewState(int stateId);

        void onRideStart();

        void onRideFinish();

        void onError(int newStateId);

        void onOk();

        void onServiceConnected();

        void onServiceDisconnected();
    }
}
