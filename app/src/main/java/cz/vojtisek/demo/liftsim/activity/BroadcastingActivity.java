package cz.vojtisek.demo.liftsim.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import cz.vojtisek.demo.liftsim.R;
import cz.vojtisek.demo.liftsim.utils.Constants;
import cz.vojtisek.demo.liftsim.utils.Log;

public class BroadcastingActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v(this, "onCreate");
        setContentView(R.layout.activity_broadcasting);
    }

    @Override
    public void onBackPressed() {
        /**
         * It is silly to go back in Broadcasting state. We must stay here - there is no CANCEL_BROADCAST option.
         */
        Toast.makeText(this, "You can't cancel the broadcast!", Toast.LENGTH_SHORT).show();
        /*setResult(RESULT_CANCELED, new Intent()
                .putExtra(MainActivity.EXTRA_NEW_STATE, -1));
        super.onBackPressed();*/
    }

    @Override
    protected void applyNewState(int stateId) {
        Log.d(this, "applyNewState " + stateId);

        switch (stateId) {
            default:
                stateId = -1;
            case Constants.STATE_WAITING:
            case Constants.STATE_RIDE:
                setResult(RESULT_CANCELED, new Intent()
                        .putExtra(MainActivity.EXTRA_NEW_STATE, stateId));
                finish();
                break;
            case Constants.STATE_BROADCASTING:
                //NOOP
                break;
        }
    }
}
